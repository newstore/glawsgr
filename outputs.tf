output "name" {
  description = <<-DESC
  The name granted to resources.
  DESC
  value       = local.name
}

output "role" {
  description = <<-DESC
  The ARN of the instance profile role created by the module. Use this to
  attach extra policies.
  DESC
  value       = aws_iam_role.this.arn
}

output "role_name" {
  description = "The name of the instance profile role created by the module."
  value       = aws_iam_role.this.name
}

output "security_group_ids" {
  description = <<-DESC
  The IDs of the security groups used by the instances. Use these to assign
  additional security group rules.
  DESC
  value       = aws_launch_template.this.vpc_security_group_ids
}

output "autoscaling_group" {
  description = <<-DESC
  The ARN of the AutoScaling group which manages the runner instances.
  DESC
  value       = aws_autoscaling_group.this.arn
}

output "launch_template" {
  description = <<-DESC
  The ARN of the Launch Template which defines the instances
  DESC
  value       = aws_launch_template.this.arn
}

output "ssh_key" {
  description = <<-DESC
  The ARN of the SSH Key Pair of the instances' default user.
  DESC
  value       = aws_key_pair.this.arn
}

output "ssh_key_secret" {
  description = <<-DESC
  The ARN of a SecretsManager Secret which holds the SSH private key that can
  be used to log into the instances.
  DESC
  value       = aws_secretsmanager_secret_version.ssh.arn
}
