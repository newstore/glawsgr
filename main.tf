# Caller identity is used to determine the sub-account into which we should
# deploy resources. If this is different for some reason, you'll need to
# assume a role in that account.
data "aws_caller_identity" "current" {}

# Suitable instance type
data "aws_ec2_instance_type" "this" {
  instance_type = var.instance_type
}

# An Amazon Linux 2 image
data "aws_ami" "this" {
  owners      = ["amazon"]
  most_recent = true
  filter {
    name   = "name"
    values = [var.ami]
  }
  filter {
    name   = "architecture"
    values = data.aws_ec2_instance_type.this.supported_architectures
  }
}

# Assume-Role policy for the role; allows usage as an instance profile (by
# allowing sts:AssumeRole to ec2.amazonaws.com).
data "aws_iam_policy_document" "assume" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# A default restrictive policy for the instance profile role.
data "aws_iam_policy_document" "default" {
  statement {
    effect    = "Deny"
    actions   = ["*"]
    resources = ["*"]
  }
}

# Pull token secret from AWS SSM
data "aws_ssm_parameter" "token" {
  name = var.ssm_parameter_name
}

# The default VPC, used if no other VPC is specified.
data "aws_vpc" "default" {
  default = true
}

# The default subnets, used if no others are specified.
data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

# A pseudo-random name to use unless overridden.
resource "random_pet" "name" {}

# This role will run the jobs.
resource "aws_iam_role" "this" {
  path               = var.iam_path
  name               = local.name
  assume_role_policy = data.aws_iam_policy_document.assume.json
}

# A key used to access instances created by the launch template
resource "aws_key_pair" "this" {
  key_name   = local.name
  public_key = coalesce(var.public_key, tls_private_key.default.public_key_openssh)
}

# A secret to hold the generated SSH private key.
resource "aws_secretsmanager_secret" "ssh" {
  name = local.name
  tags = { Name = local.name }
}

# A version to hold the generated SSH private key
resource "aws_secretsmanager_secret_version" "ssh" {
  secret_id     = aws_secretsmanager_secret.ssh.id
  secret_string = tls_private_key.default.public_key_openssh
}

# The permissions granted to the role
resource "aws_iam_policy" "this" {
  path   = var.iam_path
  name   = local.name
  policy = coalesce(var.policy, data.aws_iam_policy_document.default.json)
}

# Attaches the policy to the role
resource "aws_iam_role_policy_attachment" "this" {
  role       = aws_iam_role.this.name
  policy_arn = aws_iam_policy.this.arn
}

# The instance profile of EC2 instances launched by the launch template.
resource "aws_iam_instance_profile" "this" {
  path = var.iam_path
  name = local.name
  role = aws_iam_role.this.name
}

# The launch template used to create new runner instances.
resource "aws_launch_template" "this" {
  name                   = local.name
  instance_type          = data.aws_ec2_instance_type.this.instance_type
  image_id               = data.aws_ami.this.id
  vpc_security_group_ids = [coalesce(var.security_group, aws_security_group.default.id)]
  key_name               = aws_key_pair.this.key_name
  update_default_version = true
  block_device_mappings {
    device_name = one(data.aws_ami.this.block_device_mappings.*.device_name)
    ebs {
      volume_size = var.volume_size
    }
  }
  iam_instance_profile {
    arn = aws_iam_instance_profile.this.arn
  }
  tag_specifications {
    resource_type = "instance"
    tags          = coalesce(var.aws_tags, { Name = local.name })
  }
  capacity_reservation_specification {
    capacity_reservation_preference = "open"
  }
  tag_specifications {
    resource_type = "volume"
    tags          = coalesce(var.aws_tags, { Name = local.name })
  }
  monitoring {
    enabled = true
  }
  user_data = base64encode(templatefile("${path.module}/runner.sh.tmpl", {
    endpoint    = var.endpoint
    token       = data.aws_ssm_parameter.token.value
    description = coalesce(var.runner_description, local.name)
    tags = join(",", coalescelist(var.runner_tags, [
      local.name,
      data.aws_ami.this.architecture,
      data.aws_ami.this.name,
      data.aws_ec2_instance_type.this.instance_type,
    ]))
  }))
}

# The auto-scaling group used to ensure there are always running instances.
resource "aws_autoscaling_group" "this" {
  name                = local.name
  max_size            = var.max
  min_size            = var.min
  desired_capacity    = var.desired
  vpc_zone_identifier = coalesce(var.subnets, data.aws_subnets.default.ids)

  launch_template {
    id      = aws_launch_template.this.id
    version = "$Latest"
  }
}

# A key to be used if var.ssh_key isn't provided.
resource "tls_private_key" "default" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

# A security group to use by default
resource "aws_security_group" "default" {
  name        = local.name
  description = "Default group for ${local.name}"
  tags        = { Name = local.name }
}

# A rule for the default security group's egress
resource "aws_security_group_rule" "egress" {
  security_group_id = aws_security_group.default.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
  ipv6_cidr_blocks  = ["::/0"]
  description       = "Allow access to the internet"
}

locals {
  account = data.aws_caller_identity.current.account_id
  name    = coalesce(var.name, random_pet.name.id)
}
