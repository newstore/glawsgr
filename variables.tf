variable "namespace" {
  description = <<-DESC
  A namespace to use for IAM resource, etc, in the namespace. It defaults to a
  random value, but you might want to use the project or group name, or the
  environment, for example.
  DESC
  type        = string
  default     = null
}

variable "name" {
  description = <<-DESC
  A name for resources created by this module. It defaults to a random value.
  DESC
  type        = string
  default     = null
}

variable "endpoint" {
  description = <<-DESC
  The endpoint at which the runners will register. Change this if you're using
  a self-hosted GitLab instance.
  DESC
  type        = string
  default     = "https://gitlab.com"
}

variable "desired" {
  description = <<-DESC
  The preferred number of EC2 runner instances to run (technically the desired
  capacity of the autoscaling group). If this is zero, no EC2 runners will be
  started.
  DESC
  type        = number
  default     = 2
}

variable "min" {
  description = <<-DESC
  The minimum allowable EC2 runner instances to run.
  DESC
  type        = number
  default     = 1
}

variable "max" {
  description = <<-DESC
  The maximum allowable EC2 runner instances to run.
  DESC
  type        = number
  default     = 3
}

variable "ami" {
  description = <<-DESC
  A pattern used to choose an Amazon Machine Image for runner instances.
  See a list of instance types with `aws ec2 describe-images`. The default
  chooses the latest Amazon Linux 2 image.
  DESC
  type        = string
  default     = "amzn2-ami-hvm*"
}

variable "instance_type" {
  description = <<-DESC
  The instance type to use for EC2 runner instances. The default should be fine
  for general use, and costs about $0.04 per instance per hour.
  DESC
  type        = string
  default     = "t3.medium"
}

variable "volume_size" {
  description = <<-DESC
  The preferred size (in GiB) of the root volume's disk.
  DESC
  type        = number
  default     = 60
}

variable "subnets" {
  description = <<-DESC
  The IDs of the VPC subnets into which to deploy the EC2 runners. If left
  blank, then the account's default VPC's subnets will be detected and used.
  DESC
  type        = list(string)
  default     = null
}

variable "aws_tags" {
  description = <<-DESC
  Tags to add to all AWS resources.
  DESC
  type        = map(string)
  default     = null
}

variable "runner_tags" {
  description = <<-DESC
  Tags to add to all GitLab runners. Use these if you need to distinguish,
  say, between different environments.
  DESC
  type        = list(string)
  default     = null
}

variable "runner_description" {
  description = <<-DESC
  A description to show next to each runner in the GitLab UI.
  DESC
  type        = string
  default     = null
}

variable "policy" {
  description = <<-DESC
  Policies to grant to the EC2 instance profile role (specified by ARN). If
  left blank, the very restrictive default policy is used, so the runner can't
  really do useful things out of the box. You can create one using an
  `aws_iam_policy_document` resource and pass its json to this variable.
  DESC
  type        = string
  default     = null
}

variable "public_key" {
  description = <<-DESC
  An SSH public key installed onto all runner instances. Allows holders of the
  corresponding private key to gain access. If not supplied, a new key-pair
  will be generated and the private key is stored in an AWS SecretsManager
  secret.
  DESC
  type        = string
  default     = null
}

variable "security_group" {
  description = <<-DESC
  An AWS security groups (id) for the instances. If left blank, a default
  security groups will be used, which simply provide egress for the instances
  and SSH from anywhere.
  DESC
  type        = string
  default     = null
}

variable "iam_path" {
  description = <<-DESC
  Use this to scope the IAM resources created by this module, so you control
  access by resources created elsewhere.
  DESC
  type        = string
  default     = "/"
}

variable "ssm_parameter_name" {
  description = "The name of the AWS SSM parameter where the GitLab registration token secret is stored."
  type        = string
}
