terraform {
  required_providers {
    test = {
      source = "terraform.io/builtin/test"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
  }
}

data "gitlab_project" "this" {
  path_with_namespace = "newstore/glawsgr"
}

module "defaults" {
  source = "../.."
  token  = data.gitlab_project.this.runners_token
}
