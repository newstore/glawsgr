# newstore/glawsgr/examples/additional-permissions

Demonstrates use of the [glawsgr][1] terraform module to create runners which
have additional AWS capabilities. Having runners like these within your AWS
infrastructure gives tighter control over credentials, and eliminates the need
to provide access-keys to public services (such as GitLab).

> Before running this example, you will need to provide a `GITLAB_TOKEN` and
> AWS environment variables, to authenticate with GitLab and AWS.

This example creates runners which can read from a secret. You can test it
(in a properly configured environment 👆)

```sh
terraform init
terraform plan
terraform apply
```

> Don't forget to run `terraform destroy` when you're finished.

[glawsgr]: https://gitlab.com/
