# glawsgr/examples/addtional-permissions
# A runner with example IAM permissions

module "runners" {
  source             = "git::https://gitlab.com/newstore/glawsgr?ref=1.3.2"
  name               = "gitlab-runner-name" # Name of the Gitlab runner
  namespace          = data.gitlab_group.mygroup.path
  ssm_parameter_name = "name_of_ssm_parameter_in_your_account" # Name of SSM parameter in your sub-account storing your Gitlab runner registration token
  desired            = 2                                       # Number of desired runners
  runner_tags        = ["tag1", "tag2"]                        # Gitlab runner tags
  instance_type      = "t3.medium"                             # Default value of instance_type is t3.medium. Feel free to change.
}

data "gitlab_group" "mygroup" {
  full_path = "newstore/engineering/Infrastructure" # Path to your gitlab group
}

resource "aws_iam_policy" "policy" {
  name_prefix = "gitlab-runner-policy"
  description = "Grants permission to read the foo secret"
  policy      = data.aws_iam_policy_document.gitlab_runner_policy.json
}

# Example of an IAM policy. Adjust according to your needs.
data "aws_iam_policy_document" "gitlab_runner_policy" {
  statement {
    sid = "1"

    actions = [
      "s3:ListAllMyBuckets",
      "s3:GetBucketLocation",
    ]

    resources = [
      "arn:aws:s3:::*",
    ]
  }

  statement {
    actions = [
      "s3:ListBucket",
    ]

    resources = [
      "arn:aws:s3:::example",
    ]

    condition {
      test     = "StringLike"
      variable = "s3:prefix"

      values = [
        "",
        "home/",
        "home/&{aws:example}/",
      ]
    }
  }
}

resource "aws_iam_role_policy_attachment" "foo" {
  role       = module.runners.role_name
  policy_arn = aws_iam_policy.policy.arn

}

terraform {
  required_version = "~> 1.1.0"
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 3.8.0"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0.0"
    }
  }
}

