export default async () => {
  const targets = [...document.querySelectorAll('#version')]
  const resp = await fetch('version.txt')
  const text = await resp.text()
  const a = document.createElement('a')
  a.href = `//gitlab.com/newstore/glawsgr/-/commit/${text.trim()}`
  a.append(document.createTextNode(text.trim()));
  targets.forEach((e) => {
    while (e.hasChildNodes()) e.removeChild(e.firstChild)
    e.append(a)
  })
}
