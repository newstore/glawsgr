import showVersion from './version.js'

const GREETING = `Welcome, developer!`

addEventListener('load', ({ target, currentTarget}) => {
  showVersion()
  console.debug(`Welcome, developer!`)
  currentTarget.Korb = new Korb(currentTarget)
})
