terraform {
  required_version = ">= 1.1.0"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">= 3.8.0"
    }

    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }

    http = {
      source  = "hashicorp/http"
      version = "~> 2.1"
    }

    tls = {
      source  = "hashicorp/tls"
      version = "~> 3.1"
    }

    random = {
      source  = "hashicorp/random"
      version = ">= 3.0"
    }
  }
}
