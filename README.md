# GitLab AWS Group Runners (`glawsgr`)

An opinionated Terraform module to create GitLab runners for a given group (or
project).

## Requirements

* A [GitLab token][1] (of a group/project `Maintainer` with at least `api`
  scope)
* An [AWS session][2] with sufficient access to create new roles and policies

You can set the `AWS_*` environment variables for the latter, or use a default
profile.

## Usage

# Prerequisites
- Generate a personal access token with short validty time with API permissions, you will have to use the token when running terraform. DO NOT COMMIT THIS TOKEN! TOKEN SHOULD EXPIRE AS SOON AS IT'S NOT NEEDED ANYMORE.
- Obtain a new Gitlab runner registration token from your Gitlab group, store it in AWS SSM parameter in your sub-account.
- Deploy your runner

**Deployment is as sample as...**
- Save Gitlab registration token in AWS SSSM parameter store.
- Export AWS credentials with your favorite SSO tool (saml2aws, aws-okta, etc..).
- Use `main.tf` from `example/additional-permissions` and adjust it accordingly.
- Run `terraform init` & `terraform plan` and provide your personal Gitlab access token.
Creating a `main.tf`, copy-pasting the code below, creating AWS session with your favourite SSO tool (aws-okta or saml2aws) and running `terraform init` and then apply.


**Terraform module will deploy:**
- EC2 instances managed by autoscaling group with latest AMI
- Gitlab runner with latest version
- Gitlab runner will not have any inbound rules

## AWS Session

The *module* uses the `hashicorp/aws` provider, which honours the `AWS_*`
environment variables if they are set.

The *runners* run on EC2 instances with an Instance Profile which has a role
associated with it. You can use this to grant permissions to your runner jobs
to perform various tasks on your behalf. You can, of course, also pass AWS
credentials to the runners as part of the jobs, but this isn't recommended
normally. Nonetheless, here's an example of using push options to pass your own
session credentials to a job (provided the variables are set in the environment
already using `aws sts assume-role` or something):

    git push -o ci.variable='AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID' \
             -o ci.variable='AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY' \
             -o ci.variable='AWS_SESSION_TOKEN=$AWS_SESSION_TOKEN'

You can use this method to initially run the job on a shared runner to create
the gitlab runners, then subsequently use job tags to target the new runners
which will run with the role of the instance profile.

## Caveats

* The runners do not deregister on shutdown - if you destroy a group of
  runners, you will need to clean them up in the GitLab UI or API.
* The initialisation could be a bit smarter - for example, it would be neat to
  authorize the public SSH keys of all maintainers of the group/project.
* Monitoring is a bit of a chore - future versions may run an API to expose the
  status of the runners per instance.
* The runner registration itself is a bit outdated. There's a new
  `gitlab_runner` resource available in later version of the gitlab terraform
  provider which allow a more elegant registration/de-registration mechanism.
  This would decouple the creation of the runners from the registration. See
  [here][4].

[1]: https://docs.gitlab.com/ee/security/token_overview.html
[2]: #AWS-Session
[3]: https://docs.gitlab.com/ee/ci/yaml/index.html#tags
[4]: https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/runner
[5]: https://gitlab.com/newstore/glawsgr/-/infrastructure_registry
